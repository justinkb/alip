# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Based in part upon gnulib-9999-r1.ebuild which is:
#   Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Library of common routines intended to be shared at the source level"
DESCRIPTION="
Gnulib takes a different approach. Its components are intended to be shared at
the source level, rather than being a library that gets built, installed, and
linked against. Thus, there is no distribution tarball; the idea is to copy
files from Gnulib into your own source tree.
"
HOMEPAGE="http://www.gnu.org/software/gnulib"
DOWNLOADS=""
SCM_REPOSITORY="git://git.savannah.gnu.org/gnulib.git"
require scm-git

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

src_compile() {
    emake -C doc info html || die "emake failed"
}

src_install() {
    insinto /usr/share/${PN}
    doins -r lib
    doins -r m4
    doins -r modules
    doins -r build-aux
    doins -r top
    doins -r tests

    # install scripts
    exeinto /usr/share/${PN}
    for exe in gnulib-tool check-module posix-modules; do
        doexe $exe
    done

    # create and install wrappers
    dodir /usr/bin
    for exe in gnulib-tool check-module posix-modules; do
        dosym /usr/share/${PN}/$exe /usr/bin/$exe
    done

    emagicdocs
    doinfo doc/gnulib.info
    docinto html
    dodoc doc/gnulib.html
}
