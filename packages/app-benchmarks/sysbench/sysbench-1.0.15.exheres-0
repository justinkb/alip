# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=akopytov tag=${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Modular, cross-platform and multi-threaded benchmark tool"
DESCRIPTION="
SysBench is a modular, cross-platform and multi-threaded benchmark tool for
evaluating OS parameters that are important for a system running a database
under intensive load.
The idea of this benchmark suite is to quickly get an impression about system
performance without setting up complex database benchmarks or even without
installing a database at all.  Current features allow to test the following
system parameters:
* file I/O performance
* scheduler performance
* memory allocation and transfer speed
* POSIX threads implementation performance
* database server performance (OLTP benchmark)
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-lang/LuaJIT
        dev-libs/ConcurrencyKit
        dev-libs/libaio
    test:
        dev-util/cram
"

BUGS_TO="alip@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-system-ck
    --with-system-luajit
    --without-mysql
    --without-pgsql
)

src_prepare() {
    # Prevent using bundled cram for tests
    edo rm -r ${WORK}/third_party/cram
    autotools_src_prepare
}

